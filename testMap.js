const testMapModule = require("./map")

const items = [1, 2, 3, 4, 5, 5];

function cb(element) {
    return element;     //Just implementing the map() function
}

const result = testMapModule.map(items, cb)

console.log(result);
