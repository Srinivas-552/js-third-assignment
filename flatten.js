function flatten(nestedArr) {
    let flatArr = [];

    for (let i=0; i<nestedArr.length; i++){
        if (Array.isArray(nestedArr[i])) {      //Checking nestedArr[i] is array or not ?
            flatArr.push(...flatten(nestedArr[i]))  //Spread operator- Expands the array into individual elements
        }
        else{
            flatArr.push(nestedArr[i])
        }
    }
    return flatArr;
}

module.exports = { flatten }