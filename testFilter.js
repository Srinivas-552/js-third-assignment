const testFilterModule = require("./filter")

const items = [1,2,3,4,5,5];


function cb(arrElement) {
    if (arrElement > 1){    //Testing arrElement > 1
        return true;
    }
}


const result = testFilterModule.filter(items, cb)

console.log(result)     // output: [ 2, 3, 4, 5, 5 ]
