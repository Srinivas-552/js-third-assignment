

const output = [].reduce(() => {}, [] );
console.log(output);       // []

const output = [].reduce(() => {}, 13.8 );
console.log(output);    // 13.8
