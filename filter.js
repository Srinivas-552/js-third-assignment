// The filter() method creates a new array with array elements that passes a test.

function filter(inputArr, myCallBackFn) {
    let resultArr = [];

    for (let i=0; i<inputArr.length; i++){
        const currentEle = inputArr[i]

        const returnedValueFromCb = myCallBackFn(currentEle)

        if (returnedValueFromCb) {
            resultArr.push(currentEle)
        }
    }

    if (!resultArr){
        return [];
    } else {
        return resultArr;
    }
}

module.exports = { filter }