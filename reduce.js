
function reduce(inputArr, myCallBackFn, startingValue) {

    if (!inputArr && startingValue===null){
        return null;
    }
    else if (!inputArr && startingValue==={}){
        return {};
    }
    else {
        if (startingValue === undefined){
            startingValue = inputArr[0];
            let total = startingValue;
            for (let i=1; i<inputArr.length; i++){
                const arrElement = inputArr[i];
    
                const prevValue =  myCallBackFn(startingValue, arrElement)  // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
                startingValue = prevValue;
                total = prevValue;

            }
            return total;

        }else {
            let total = startingValue
            for (let j=0; j<inputArr.length; j++){
                const arrElement = inputArr[j];
                const prevValue = myCallBackFn(startingValue, arrElement)
                startingValue = prevValue;
                total = prevValue;
            }
            return total;
        }
    }
}

module.exports = { reduce }


