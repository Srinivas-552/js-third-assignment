const testFlattenModule = require("./flatten")

const nestedArray = [1, [2], [[3]], [[[4]]]];


const result = testFlattenModule.flatten(nestedArray)

console.log(result)     //[ 1, 2, 3, 4 ]
