
function each(inputArr, myCallBackFn) {
    // console.log(inputArr)
    for (let i=0; i<inputArr.length; i++){
        const element = inputArr[i];
        myCallBackFn(element, i);
    }
}

module.exports = { each }