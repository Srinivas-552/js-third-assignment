
function map(inputArr, myCallBackFn) {
    const newArray = [];
    if (!inputArr){
        return [];
    }
    for (let i=0; i<inputArr.length; i++){
        const element = inputArr[i];
        const returnedEle = myCallBackFn(element)
        newArray.push(returnedEle);
    }
    return newArray;
}

module.exports = { map }