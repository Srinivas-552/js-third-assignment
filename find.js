// The find() method returns the value of the first array element that passes a test function

function find(inputArr, myCallBackFn) {
    let returnedValueFromCb;
    for (let i=0; i<inputArr.length; i++){
        const arrElement = inputArr[i]

        returnedValueFromCb = myCallBackFn(arrElement)
        if (returnedValueFromCb) {
            return arrElement;
        }
    }
    if (!returnedValueFromCb){
        return undefined;
    }
}

module.exports = { find }